# This file is part sale_shop module for Tryton.
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from decimal import Decimal

from trytond import backend
from trytond.model import fields, Unique, ModelView
from trytond.transaction import Transaction
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Bool, Eval, Or
import logging
from trytond.report import Report
from trytond.wizard import Wizard, StateView, StateReport, Button

__all__ = ['Sale', 'SaleBySupplierStart', 'SaleBySupplier',
    'PrintSaleBySupplier', 'SaleLine']

logger = logging.getLogger(__name__)


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'

    @fields.depends('product', 'unit', 'quantity', 'description',
        '_parent_sale.party', '_parent_sale.currency',
        '_parent_sale.sale_date')
    def on_change_product(self):
        super(SaleLine, self).on_change_product()
        config = Pool().get('sale.configuration')(1)
        if config.use_description_product and self.product and self.product.description:
            self.description = self.product.template.name + ' | ' + self.product.description


class SaleBySupplierStart(ModelView):
    'Sale By Supplier Start'
    __name__ = 'sale_shop.sale_by_supplier.start'
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    supplier = fields.Many2One('party.party', 'Supplier', required=True)
    company = fields.Many2One('company.company', 'Company',
            required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_end_date():
        Date_ = Pool().get('ir.date')
        return Date_.today()


class PrintSaleBySupplier(Wizard):
    'Sale By Supplier'
    __name__ = 'sale_shop.print_sale_by_supplier'
    start = StateView('sale_shop.sale_by_supplier.start',
        'sale_shop.print_sale_by_supplier_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True),
            ])
    print_ = StateReport('sale_shop.sale_by_supplier_report')

    def do_print_(self, action):

        data = {
            'company': self.start.company.id,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            'supplier': self.start.supplier.id,
        }
        return action, data

    def transition_print_(self):
        return 'end'

class SaleBySupplier(Report):
    'Sale By Supplier Report'
    __name__ = 'sale_shop.sale_by_supplier_report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(SaleBySupplier, cls).get_context(records, data)
        pool = Pool()
        Company = pool.get('company.company')
        ProductSupplier = pool.get('purchase.product_supplier')
        SaleLine = pool.get('sale.line')
        Party = pool.get('party.party')
        company = Company(data['company'])
        party = Party(data['supplier'])
        products_suppliers = ProductSupplier.search([
            ('party', '=', data['supplier']),
            ])
        templates = [p.product for p in products_suppliers]
        records = {}
        total = []
        lines = SaleLine.search([
            ('product.template', 'in', templates),
            ('sale.sale_date', '>=', data['start_date']),
            ('sale.sale_date', '<=', data['end_date']),
            ])
        for line in lines:
            if line.product not in records.keys():
                records[line.product] = {
                    'code': line.product.code,
                    'name': line.product.template.name,
                    'quantity': [],
                    'amount': [],
                }
            records[line.product]['quantity'].append(line.quantity)
            records[line.product]['amount'].append(line.amount)
            total.append(line.amount)

        report_context['records'] = records.values()
        report_context['start_date'] = data['start_date']
        report_context['end_date'] = data['end_date']
        report_context['supplier'] = party.name
        report_context['company'] = company
        report_context['total'] = sum(total)
        return report_context


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'
    shop = fields.Many2One('sale.shop', 'Shop', required=True, domain=[
            ('id', 'in', Eval('context', {}).get('shops', [])),
            ],
        states={
            'readonly': Or(Bool(Eval('number')), Bool(Eval('lines'))),
            }, depends=['number', 'lines'])
    shop_address = fields.Function(fields.Many2One('party.address',
            'Shop Address'),
        'on_change_with_shop_address')

    @classmethod
    def __setup__(cls):
        super(Sale, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints.extend([
            ('number_uniq', Unique(t, t.shop, t.number),
                'There is another sale with the same number.\n'
                'The number of the sale must be unique!')
            ])

        shipment_addr_domain = cls.shipment_address.domain[:]
        if shipment_addr_domain:
            cls.shipment_address.domain = [
                'OR',
                shipment_addr_domain,
                [('id', '=', Eval('shop_address', 0))],
                ]
        else:
            cls.shipment_address.domain = [('id', '=', Eval('shop_address'))]
        cls.shipment_address.depends.append('shop_address')
        cls.currency.states['readonly'] = Eval('state') != 'draft'
        cls.currency.depends.append('shop')

        cls._error_messages.update({
            'not_sale_shop': (
                'Go to user preferences and select a shop ("%s")'),
            'no_return_sequence': (
                'Return Sale Sequence is missing!'),
            'sale_not_shop': (
                'Sale have not related a shop'),
            'edit_sale_by_shop': ('You cannot edit this order because you '
                'do not have permission to edit in this shop.'),
        })


    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        table = TableHandler(cls, module_name)
        # if not table.column_exist('shop'):
        #     table.add_raw_column(
        #             'shop',
        #             cls.shop.sql_type(),
        #             cls.shop.sql_format, None, None
        #             )
        #     logger.warning(
        #         'Create a Sale Shop and assign current sales with this shop '
        #             'with SQL:')
        #     logger.warning(
        #         'UPDATE sale_sale set shop = 1;')

        # Migration from 3.8: remove reference constraint
        if not table.column_exist('number'):
            table.drop_constraint('reference_uniq')

        super(Sale, cls).__register__(module_name)

    @staticmethod
    def default_company():
        User = Pool().get('res.user')
        user = User(Transaction().user)
        return user.shop.company.id if user.shop else \
            Transaction().context.get('company')

    @staticmethod
    def default_shop():
        User = Pool().get('res.user')
        user = User(Transaction().user)
        return user.shop.id if user.shop else None

    @staticmethod
    def default_invoice_method():
        User = Pool().get('res.user')
        user = User(Transaction().user)
        if not user.shop:
            Config = Pool().get('sale.configuration')
            config = Config(1)
            return config.sale_invoice_method
        return user.shop.sale_invoice_method

    @staticmethod
    def default_description():
        config = Pool().get('sale.configuration')(1)
        if config.default_description:
            return config.default_description

    @staticmethod
    def default_shipment_method():
        User = Pool().get('res.user')
        user = User(Transaction().user)
        if not user.shop:
            Config = Pool().get('sale.configuration')
            config = Config(1)
            return config.sale_shipment_method
        return user.shop.sale_shipment_method

    @staticmethod
    def default_warehouse():
        User = Pool().get('res.user')
        user = User(Transaction().user)
        if user.shop:
            return user.shop.warehouse.id
        else:
            Location = Pool().get('stock.location')
            return Location.search([('type', '=', 'warehouse')], limit=1)[0].id

    @staticmethod
    def default_price_list():
        User = Pool().get('res.user')
        user = User(Transaction().user)
        return user.shop.price_list.id if user.shop else None

    @staticmethod
    def default_payment_term():
        User = Pool().get('res.user')
        user = User(Transaction().user)
        return user.shop.payment_term.id if user.shop else None

    @staticmethod
    def default_shop_address():
        User = Pool().get('res.user')
        user = User(Transaction().user)
        return (user.shop and user.shop.address and
            user.shop.address.id or None)

    @fields.depends('shop', 'party')
    def on_change_shop(self):
        if not self.shop:
            return
        for fname in ('company', 'warehouse', 'currency', 'payment_term'):
            fvalue = getattr(self.shop, fname)
            if fvalue:
                setattr(self, fname, fvalue.id)
        if ((not self.party or not self.party.sale_price_list)
                and self.shop.price_list):
            self.price_list = self.shop.price_list.id
        if self.shop.sale_invoice_method:
            self.invoice_method = self.shop.sale_invoice_method
        if self.shop.sale_shipment_method:
            self.shipment_method = self.shop.sale_shipment_method

    @fields.depends('shop')
    def on_change_with_shop_address(self, name=None):
        return (self.shop and self.shop.address and
            self.shop.address.id or None)

    @fields.depends('shop', 'party')
    def on_change_party(self):
        super(Sale, self).on_change_party()
        if self.shop:
            if not self.price_list and self.invoice_address:
                self.price_list = self.shop.price_list.id
                self.price_list.rec_name = self.shop.price_list.rec_name
            if not self.payment_term and self.invoice_address:
                self.payment_term = self.shop.payment_term.id
                self.payment_term.rec_name = self.shop.payment_term.rec_name

    @classmethod
    def set_number(cls, sales):
        '''
        Fill the number field with the sale shop or sale config sequence
        '''
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('sale.configuration')
        User = Pool().get('res.user')
        config = Config(1)
        user = User(Transaction().user)
        for sale in sales:
            if sale.number:
                continue
            elif sale.shop:
                if sale.total_amount >= Decimal('0'):
                    number = Sequence.get_id(sale.shop.sale_sequence.id)
                else:
                    if not sale.shop.sale_return_sequence:
                        sale.raise_user_error('no_return_sequence')
                    number = Sequence.get_id(sale.shop.sale_return_sequence.id)
            elif user.shop:
                number = Sequence.get_id(user.shop.sale_sequence.id)
            else:
                number = Sequence.get_id(config.sale_sequence.id)
            cls.write([sale], {
                    'number': number,
                    })

    @classmethod
    def create(cls, vlist):
        vlist2 = []
        for vals in vlist:
            User = Pool().get('res.user')
            user = User(Transaction().user)
            vals = vals.copy()
            if not 'shop' in vals:
                if not user.shop:
                    cls.raise_user_error('not_sale_shop', (
                            user.rec_name,)
                            )
                vals['shop'] = user.shop.id
            vlist2.append(vals)
        return super(Sale, cls).create(vlist2)

    @classmethod
    def write(cls, *args):
        '''
        Only edit Sale users available edit in this shop
        '''
        User = Pool().get('res.user')
        user = User(Transaction().user)
        if user.id != 0:
            actions = iter(args)
            for sales, _ in zip(actions, actions):
                for sale in sales:
                    if not sale.shop:
                        cls.raise_user_error('sale_not_shop')
                    if not sale.shop in user.shops:
                        cls.raise_user_error('edit_sale_by_shop')
        super(Sale, cls).write(*args)

    def _get_invoice_sale(self):
        invoice = super(Sale, self)._get_invoice_sale()
        invoice.reference = self.reference
        invoice.description = self.description
        invoice.comment = self.comment
        invoice.shop = self.shop.id
        return invoice
