# This file is part Invoice_shop module for Tryton.
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
# from trytond.transaction import Transaction

__all__ = ['Invoice']


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'
    shop = fields.Many2One('sale.shop', 'Shop', required=False, domain=[
            ('id', 'in', Eval('context', {}).get('shops', [])),
            ], depends=['number'])

    # @staticmethod
    # def default_shop():
    #     User = Pool().get('res.user')
    #     user = User(Transaction().user)
    #     return user.shop.id if user.shop else None
