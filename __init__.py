# This file is part sale_shop module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
import shop
import sale
import user
import configuration
import stock
import invoice


def register():
    Pool.register(
        configuration.Configuration,
        shop.SaleShop,
        shop.SaleShopResUser,
        user.User,
        sale.Sale,
        stock.ShipmentOut,
        stock.ShipmentOutReturn,
        sale.SaleBySupplierStart,
        sale.SaleLine,
        invoice.Invoice,
        module='sale_shop', type_='model')
    Pool.register(
        sale.PrintSaleBySupplier,
        module='sale_shop', type_='wizard')
    Pool.register(
        sale.SaleBySupplier,
        module='sale_shop', type_='report')
