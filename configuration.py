#The COPYRIGHT file at the top level of this repository contains the full
#copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta

__all__ = ['Configuration']


class Configuration(metaclass=PoolMeta):
    __name__ = 'sale.configuration'
    default_description = fields.Char('Default Description')
    use_description_product = fields.Boolean('Use Description Product')
    default_invoice_type = fields.Char('Default Invoice Type')
    cache_products_local = fields.Boolean('Cache Products Local')

    @staticmethod
    def default_cache_products_local():
        return True
